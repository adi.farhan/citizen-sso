'use strict'
const express = require('express')
const path = require('path')
const { DecodeToken  } = require('./validation')
const app = express()

app.get('/', function (req, res) {
  const rule = DecodeToken(req,res)
  if(rule === false)
    var arr = []
  else
    var arr = rule.roles;
    arr =  arr.reduce((index,value)=> (index[value]=true,index),{});
  res.render('index',{
    request:req.headers,
    rule: arr
  })
})

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.listen(80);
console.log('app running in port 80');